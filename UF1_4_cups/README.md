# M06-ASO

## @edt ASIX M06 2021-2022

Podeu trobar les imatges docker al Dockehub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Podeu trobar la documentació i els dokerfiles del mòdul a [ASIX-M06](https://github.com/edtasixm06)

Podeu trobar la documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)


---

### Servei d'impressió CUPS
 
 * [2205-c2-ud2-annex2-cups](https://gitlab.com/edtasixm06/m06-aso/-/blob/main/UF1_4_cups/2205_c2_ud2_annex2_cups.pdf)
 * [cups-teoria](https://gitlab.com/edtasixm06/m06-aso/-/blob/main/UF1_4_cups/cups_teoria.pdf)
 * [HowTo-ASIX-Cups](https://gitlab.com/edtasixm06/m06-aso/-/blob/main/UF1_4_cups/HowTo-ASIX-Cups.pdf)
 * [LPI-108.4-manage_printers_and_printing.pdf](https://gitlab.com/edtasixm06/m06-aso/-/blob/main/UF1_4_cups/LPI-108.4-Manage_printers_and_printing.pdf)
 * [l-lpic1107-pdf](https://gitlab.com/edtasixm06/m06-aso/-/blob/main/UF1_4_cups/l-lpic1107-pdf.pdf)
